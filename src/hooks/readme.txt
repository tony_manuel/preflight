;==========================================
; Title:  Hooks
; Author: Tony Manuel
; Date:   26 Jul 2021
; Desc:   This folder consists of custom hooks which can be used across the components.
;         Eg: postHandler,getHandler,putHandler,redirectHandler,etc.
;==========================================