;==========================================
; Title:  constants
; Author: Tony Manuel
; Date:   26 Jul 2021
; Desc:   This folder consists of different files with constant values which will be used across the app.
;         Eg: AppRoutes,ApiEndPoints,Labels,MenuName, etc.
;==========================================