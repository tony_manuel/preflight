;==========================================
; Title:  Store
; Author: Tony Manuel
; Date:   26 Jul 2021
; Desc:   This folder consists of global store,configurations and the store middlewares for the app.
;         Eg: Redux,Sagas,Thunk,etc.
;==========================================