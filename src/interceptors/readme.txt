;==========================================
; Title:  Interceptors
; Author: Tony Manuel
; Date:   26 Jul 2021
; Desc:   This folder consists of one or more http client interceptors for api requests.
;         Interceptors are usefull if the app needs to communicate with multiple microservices
;         where each microservices requires certain headers to be passed.
;         Eg: Interceptor for authentications, Interceptor for data requests,etc.
;==========================================