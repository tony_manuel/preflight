;==========================================
; Title:  Helper
; Author: Tony Manuel
; Date:   26 Jul 2021
; Desc:   This folder consists of different helper files with various functions which
;         will be used across the app.
;         Eg: Validations,Date_Convertions,Path_Resolvers,etc.
;==========================================