;==========================================
; Title:  form-utils
; Author: Tony Manuel
; Date:   26 Jul 2021
; Desc:   This folder consists of elements that can be used as the building blocks for a form.
;         Items in this folder can be refered to as Atoms in Automic Design Pattern.
          Eg: textfields,buttons,dropdown,etc.
;==========================================