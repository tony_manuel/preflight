import React from 'react';
import PropTypes from 'prop-types';
import { createUseStyles } from 'react-jss';
import Wrapper from '../Wrapper';

const useStyles = createUseStyles({
  typography: (props) => ({
    display: 'inline',
    '& *': { margin: 0, padding: 0, display: 'inline' },
    cursor: props.pointer ? 'pointer' : 'default',
  }),
});

const Typography = ({ children, ...props }) => {
  const classes = useStyles(props);
  const getComponent = (variant, name) => {
    switch (variant) {
      case 'h1':
        return <h1 name={name}>{children}</h1>;
      case 'h2':
        return <h2 name={name}>{children}</h2>;
      case 'h3':
        return <h3 name={name}>{children}</h3>;
      case 'h4':
        return <h4 name={name}>{children}</h4>;
      case 'h5':
        return <h5 name={name}>{children}</h5>;
      case 'h6':
        return <h6 name={name}>{children}</h6>;
      case 'title':
        return (
          <h2 name={name} className={classes.title}>
            {children}
          </h2>
        );
      case 'subtitle':
        return (
          <h6 name={name} className={classes.subtitle}>
            {children}
          </h6>
        );
      default:
        return <h2 name={name}>{children}</h2>;
    }
  };
  return (
    <Wrapper classes={classes.typography} onClick={props.onClick}>
      {getComponent(props.variant, props.name)}
    </Wrapper>
  );
};

Typography.defaultProps = {
  variant: 'h3',
  name: '',
  onClick: () => {},
};

Typography.propTypes = {
  variant: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.node.isRequired,
};

export default Typography;
