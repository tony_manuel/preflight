import React from 'react';
import PropTypes from 'prop-types';

const Wrapper = ({ children, classes, onClick }) => (
  <div className={classes} onClick={onClick}>
    {children}
  </div>
);

Wrapper.defaultProps = { classes: '', onClick: () => {} };

Wrapper.propTypes = {
  children: PropTypes.node.isRequired,
  classes: PropTypes.string,
  onClick: PropTypes.func,
};

export default Wrapper;
