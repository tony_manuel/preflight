import React from 'react';
import PropTypes from 'prop-types';
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles({
  input: {
    padding: '10px',
    borderRadius: '5px',
    outline: 'none',
    border: 0,
    width: '90%',
  },
});

const TextField = ({ label, value, onChange }) => {
  const classes = useStyles();

  return (
    <input
      type="text"
      value={value}
      onChange={onChange}
      placeholder={label}
      className={classes.input}
    />
  );
};

TextField.defaultProps = {
  value: '',
  onChange: () => {},
  label: '',
};

TextField.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  label: PropTypes.string,
};
export default TextField;
