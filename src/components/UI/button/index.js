import React from 'react';
import PropTypes from 'prop-types';
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles({
  button: (props) => ({
    width: props.fullWidth ? '100%' : 'auto',
    height: '35px',
    border: 'none',
    background: '#FF00CC',
    color: '#fff',
    cursor: 'pointer',
    fontWeight: 'bold',
  }),
});

const Button = ({ children, ...props }) => {
  const classes = useStyles(props);
  return (
    <button type="button" className={classes.button}>
      {children}
    </button>
  );
};

Button.defaultProps = { fullWidth: false };

Button.propTypes = {
  children: PropTypes.node.isRequired,
  fullWidth: PropTypes.bool,
};

export default Button;
