import React from 'react';
import { createUseStyles } from 'react-jss';
import PropTypes from 'prop-types';

const useStyles = createUseStyles({
  grid: {
    display: 'grid',
    gridTemplateColumns: 'repeat(12, 1fr)',
    height: '100%',
  },
});

const GridContainer = ({ children }) => {
  const classes = useStyles();
  return <div className={classes.grid}>{children}</div>;
};

GridContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default GridContainer;
