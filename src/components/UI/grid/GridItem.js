import React from 'react';
import { createUseStyles } from 'react-jss';
import PropTypes from 'prop-types';

const useStyles = createUseStyles({
  cols: (props) => ({
    display: 'grid',
    gridColumn: `span ${props.size}`,
  }),
});

const GridItem = ({ children, ...props }) => {
  const classes = useStyles(props);
  return <div className={classes.cols}>{children}</div>;
};

GridItem.defaultProps = { size: 12 };

GridItem.propTypes = {
  children: PropTypes.node.isRequired,
  size: PropTypes.number,
};

export default GridItem;
