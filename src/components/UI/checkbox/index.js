import React from 'react';
import PropTypes from 'prop-types';
import Typography from '../typography';

const CheckBox = ({ value, onChange, label }) => (
  <div style={{ display: 'flex' }}>
    <input type="checkbox" value={value} onChange={onChange} id="check" />
    <Typography variant="h5">{label}</Typography>
  </div>
);

CheckBox.defaultProps = { value: '', onChange: () => {}, label: '' };

CheckBox.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  label: PropTypes.string,
};

export default CheckBox;
