;==========================================
; Title:  Higher Order Components (hoc)
; Author: Tony Manuel
; Date:   26 Jul 2021
; Desc:   This folder consists of components which can be used as a hoc for another components.
;         Eg: AppLayout,RBAC(Role based access control),Spinners,etc.
;==========================================