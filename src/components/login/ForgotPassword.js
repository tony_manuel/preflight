import React from 'react';
import PropTypes from 'prop-types';

import { createUseStyles } from 'react-jss';
import Button from '../UI/button';
import TextField from '../UI/textfield';
import Typography from '../UI/typography';
import Wrapper from '../UI/Wrapper';

const useStyles = createUseStyles({
  loginForm: {
    height: '80%',
    margin: 'auto 50px',
    borderRadius: 30,
    background: '#082e6d',
    padding: '20px 30px',
    color: '#FFF',
    '& input': {
      marginBottom: '15px',
    },
    '& h4': {
      margin: 'revert',
      display: 'block',
    },
  },
  flex: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '10px',
  },
});

const ForgotPassword = ({ login }) => {
  const classes = useStyles();
  return (
    <Wrapper classes={classes.loginForm}>
      <Wrapper>
        <Typography variant="h4">FORGOT PASSWORD</Typography>
      </Wrapper>
      <TextField label="Email Address" />
      <Wrapper classes={classes.flex}>
        <Typography variant="h5" name="toggleLogin" onClick={login} pointer>
          Back To Login
        </Typography>
      </Wrapper>
      <Button fullWidth>RESET PASSWORD</Button>
    </Wrapper>
  );
};
ForgotPassword.propTypes = {
  login: PropTypes.func.isRequired,
};

export default ForgotPassword;
