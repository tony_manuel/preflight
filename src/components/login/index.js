import React, { useState } from 'react';
import ForgotPassword from './ForgotPassword';
import Login from './Login';

const Auth = () => {
  const [showLogin, setShowLogin] = useState(true);
  const toggleForm = (e, value) => {
    if (e.target?.attributes?.name?.value === 'toggleLogin')
      setShowLogin(value);
  };
  return showLogin ? (
    <Login forgotPwd={(e) => toggleForm(e, false)} />
  ) : (
    <ForgotPassword login={(e) => toggleForm(e, true)} />
  );
};

export default Auth;
