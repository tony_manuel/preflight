import React from 'react';
import { screen, render, fireEvent } from '@testing-library/react';
import Auth from './index';

describe('Login', () => {
  test('renders Login component', () => {
    render(<Auth />);
    expect(screen.getByText('LOGIN TO YOUR ACCOUNT')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Email Address')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Password')).toBeInTheDocument();
    expect(screen.getByText('Forgot Password?')).toBeInTheDocument();
  });

  test('Click Forgot Password', () => {
    render(<Auth />);
    fireEvent.click(screen.getByText('Forgot Password?'));
    expect(screen.getByText('FORGOT PASSWORD')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Email Address')).toBeInTheDocument();
    expect(screen.getByText('RESET PASSWORD')).toBeInTheDocument();

    fireEvent.click(screen.getByText('Back To Login'));
    expect(screen.getByText('LOGIN TO YOUR ACCOUNT')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Email Address')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Password')).toBeInTheDocument();
    expect(screen.getByText('Forgot Password?')).toBeInTheDocument();
  });
});
