import React from 'react';
import PropTypes from 'prop-types';
import { createUseStyles } from 'react-jss';
import Button from '../UI/button';
import CheckBox from '../UI/checkbox';
import TextField from '../UI/textfield';
import Typography from '../UI/typography';
import Wrapper from '../UI/Wrapper';

const useStyles = createUseStyles({
  loginForm: {
    height: '80%',
    margin: 'auto 50px',
    borderRadius: 30,
    background: '#082e6d',
    padding: '20px 30px',
    color: '#FFF',
    '& input': {
      marginBottom: '15px',
    },
    '& h4': {
      margin: 'revert',
      display: 'block',
    },
  },
  flex: {
    display: 'flex',
    justifyContent: 'space-between',
  },
});

const Login = ({ ...props }) => {
  const classes = useStyles();

  return (
    <Wrapper classes={classes.loginForm}>
      <div>
        <Typography variant="h4">LOGIN TO YOUR ACCOUNT</Typography>
      </div>

      <TextField label="Email Address" />
      <TextField label="Password" />
      <Wrapper classes={classes.flex}>
        <CheckBox label="Remember Me" />
        <Typography
          variant="h5"
          name="toggleLogin"
          onClick={props.forgotPwd}
          pointer
        >
          Forgot Password?
        </Typography>
      </Wrapper>
      <Button fullWidth>LOGIN</Button>
    </Wrapper>
  );
};

Login.defaultProps = {
  forgotPwd: () => {},
};
Login.propTypes = {
  forgotPwd: PropTypes.func,
};

export default Login;
