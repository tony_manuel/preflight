import React from 'react';
import { createUseStyles } from 'react-jss';
import Login from '../../components/login';
import GridContainer from '../../components/UI/grid/GridContainer';
import GridItem from '../../components/UI/grid/GridItem';
import Wrapper from '../../components/UI/Wrapper';

const useStyles = createUseStyles({
  wrapper: {
    height: '100vh',
  },
  banner: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    color: '#fff',
  },
});

const Authentication = () => {
  const classes = useStyles();

  return (
    <Wrapper classes={classes.wrapper}>
      <GridContainer>
        <GridItem size={8}>
          <Wrapper classes={classes.banner}>
            <img
              src={`${process.env.PUBLIC_URL}/preventx.svg`}
              alt="preventex logo"
            />
          </Wrapper>
        </GridItem>
        <GridItem size={4}>
          <Login />
        </GridItem>
      </GridContainer>
    </Wrapper>
  );
};

export default Authentication;
