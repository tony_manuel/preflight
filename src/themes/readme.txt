;==========================================
; Title:  Themes
; Author: Tony Manuel
; Date:   26 Jul 2021
; Desc:   This folder consists of UI theme configurations for the app.
;         Eg: Chakra,Material-UI,etc.
;==========================================